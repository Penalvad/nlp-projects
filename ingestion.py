# %%
import wikipediaapi
import pandas as pd
headers = {'User-Agent':'http'}
wiki = wikipediaapi.Wikipedia(language='en',headers=headers)

article = wiki.article('Buddhism')
df = pd.DataFrame(columns=['summary','text'],index=[])

# %%
df.loc[0,'summary'] = article.summary

# %%
df.loc[0,'text'] = article.text

# %%
for i,page in enumerate(article.links):
    aux = wiki.page(page)
    df.loc[i+1,'summary'] = aux.summary
    df.loc[i+1,'text'] = aux.text

# %%



